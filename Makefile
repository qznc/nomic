PUBDIR=${HOME}/public_html/nomic

default: regeln.html state.html

%.html: %.md
	pandoc -f markdown -t html $< -o $@ --standalone -c style.css -V lang=de-1996

.PHONY: clean show publish

clean:
	rm -f regeln.html

show: regeln.html
	sensible-browser regeln.html

publish: regeln.html state.html style.css
	mkdir ${PUBDIR} || true
	cp $^ ${PUBDIR}/
