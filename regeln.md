% Nomic Regeln

## Preamble

Dieses Spiel basiert auf Nomic
und besteht darin die Spielregeln zu verändern.
Gewinn- und Endbedingungen fehlen bewusst
und müssen erstmal von den Spielern eingebracht werden.

## Regeln

**0**
Der Diktator ist ...
Die Spieler sind ...

**1**
Jeder Spieler darf Anträge zur Abstimmung per E-Mail an den Diktator einreichen.
Anträge sind entweder eine Änderung einer Regel oder eine neue zusätzliche Regel.
Aber nur ein Antrag pro Spieler gleichzeitig.

**2**
Die Spieler stimmen über jeden Antrag ab.
Wenn eine Abstimmungen mindestens 51% "Pro" ist,
werden die Regeln entsprechend des Antrags geändert.

**3**
Der Diktator verwaltet den aktuellen Regelsatz für alle Spieler zugänglich.
Ebenso den Punktestand und aktuelle Anträge.

**4**
Wenn Spieler verschiedene Interpretationen einer Regel haben,
entscheidet der Diktator, welche Interpretation gültig ist.

## Ende
